chroot=chroot
pkgs=$(shell ls src | sed 's#^\(.*\)$$#public/provision/x86_64/\1.pkg.tar.zst.sig#g')

public/provision/x86_64/%.pkg.tar.zst.sig: ${chroot} src/%/PKGBUILD
	cd src/$*/ && makechrootpkg -c -r ../../${chroot} $(shell echo $^ | cut -d ' ' -f 3- | sed 's/^\(.\)/ \1/g' | sed 's# # -I ../../#g' | sed 's/.sig//g')
	gpg --detach-sign src/$*/$*-*.*.*-*-x86_64.pkg.tar.zst
	mv src/$*/$*-*.*.*-*-x86_64.pkg.tar.zst public/provision/x86_64/$*.pkg.tar.zst
	mv src/$*/$*-*.*.*-*-x86_64.pkg.tar.zst.sig public/provision/x86_64/$*.pkg.tar.zst.sig
	repo-add --sign public/provision/x86_64/provision.db.tar.gz public/provision/x86_64/$*.pkg.tar.zst

all: ${pkgs}

${chroot}:
	mkdir ${chroot}
	mkarchroot ${chroot}/root base-devel
	arch-nspawn ${chroot}/root pacman -Syu

clean:
	sudo rm -rf ${chroot}

public/provision/x86_64/provision-basesystem.pkg.tar.zst.sig: src/provision-basesystem/PKGBUILD
public/provision/x86_64/provision-code.pkg.tar.zst.sig: src/provision-code/PKGBUILD
public/provision/x86_64/provision-common.pkg.tar.zst.sig: src/provision-common/PKGBUILD public/provision/x86_64/provision-docker.pkg.tar.zst.sig public/provision/x86_64/provision-js-dev.pkg.tar.zst.sig public/provision/x86_64/provision-shell.pkg.tar.zst.sig
public/provision/x86_64/provision-desktop.pkg.tar.zst.sig: src/provision-desktop/PKGBUILD
public/provision/x86_64/provision-docker.pkg.tar.zst.sig: src/provision-docker/PKGBUILD
public/provision/x86_64/provision-js-dev.pkg.tar.zst.sig: src/provision-js-dev/PKGBUILD
public/provision/x86_64/provision-pr791.pkg.tar.zst.sig: src/provision-pr791/PKGBUILD public/provision/x86_64/provision-basesystem.pkg.tar.zst.sig public/provision/x86_64/provision-common.pkg.tar.zst.sig public/provision/x86_64/provision-docker.pkg.tar.zst.sig public/provision/x86_64/provision-js-dev.pkg.tar.zst.sig public/provision/x86_64/provision-shell.pkg.tar.zst.sig
public/provision/x86_64/provision-shell.pkg.tar.zst.sig: src/provision-shell/PKGBUILD